#!/bin/ksh
# 
# NAME
#	trim_file
#
# SYNOPSIS
#	trim_file [inputfile] [outputfile]
#
# DESCRIPTION
#	This script takes an input file and removes any blank lines and trims the white space
#	from the beginning and end of each line.
#	If no argument is given the script alerts the user with a usage message and issues
#	and exit code of one. If the input file does not exist, alert the user and issue
#	an exit code of two. If no output file is given then the default output file will 
#	be tmp.out in the current working directory.
#
# EXAMPLES
#	trim_file hg.txt hostgroup.list
#
# AUTHOR
#	ron georgia  <rong1611@gmail.com>
# HISTORY
# 	5/15/2014 Initial commit.
#
##############################################################################################

if [ $# -eq 0 ]; then
	print "\nUsage: $0 filename\n"
	exit 1
fi
if [ ! -f $1 ]; then
	print "$1 : File Not Found!"
	exit 2
else
	if [ $# -le 1 ]; then
		OUTPUT_FILE=${PWD}/tmp.out
	else
		OUTPUT_FILE=$2
	fi
	sed '/^$/d; s/^ *//;s/ *$//;s/  */ /;' $1  > $OUTPUT_FILE
fi

exit 0
